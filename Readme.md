This repository contains a python clone of a popular game,
created as a programming exercise at Marmara University for the
course Object Oriented Software Design (CSE363).

It is intended as an instructional software.

This work is licensed under a Creative Commons
Attribution-ShareAlike 4.0 International License.

Authors:
 * Peter Schueller <schueller.p@gmail.com> (2014)
