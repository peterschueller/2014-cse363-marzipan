#
# This work is licensed under a
# Creative Commons Attribution-ShareAlike 4.0
# International License.
#
# Authors:
# * Peter Schueller <schueller.p@gmail.com> (2014)
#
import random
import time

class Observable:
  def __init__(self):
    self.observers = []
    
  def notify(self):
    for o in self.observers:
      o.notify()

  def registerObserver(self,obs):
    self.observers.add(obs)

class Observer:
  def __init__(self, observable):
    self.observable = observable

  def notify(self):
    """
    this is called if observable changed
    """
    print "warning: not implemented notify() in Observer"

def refillEmptyCells(grid, numtypes):
  for y in range(0,grid.sizey):
    for x in range(0,grid.sizex):
      #print "at row %d at col %d" % (row,col)
      #print "my row is "+str(model.grid.cells[row])
      cell = grid.getCell(x,y)
      if cell.almond == None:
        removability = None
        if random.randint(0,9) == 0:
          removability = JokerRemovability()
        else:
          removability = SimpleRemovability(
            random.randint(0,SimpleRemovability.maxcolors-1))

        removeEffect = None
        if random.randint(0,50) == 0:
          removeEffect = BombRemoveEffect()
        else:
          removeEffect = DefaultRemoveEffect()
        a = Almond(cell, removability, removeEffect)
        cell.almond = a

def initializeGrid(model,sizex,sizey,numtypes):
  """
  initialize the grid in model
  with given size and number of almond types
  """
  model.grid = Grid(sizex,sizey)
  refillEmptyCells(model.grid, numtypes)

class Removability:
  def __init__(self):
    pass
  def getColorComponent(self):
    raise Exception("not implemented, must derive")
  def __str__(self):
    raise Exception("not implemented, must derive")

class SimpleRemovability(Removability):
  maxcolors = 6
  def __init__(self,color):
    Removability.__init__(self)
    #print "got color %d" % color
    assert(color >= 0 and color < SimpleRemovability.maxcolors)
    self.color = color
  def getColorComponent(self):
    zeroes = [0]*SimpleRemovability.maxcolors
    zeroes[self.color] = 1
    assert(len(zeroes) == SimpleRemovability.maxcolors)
    return zeroes
  def __str__(self):
    #print "my color "+repr(self.color)
    return str(self.color)

class JokerRemovability(Removability):
  def __init__(self):
    Removability.__init__(self)
  def getColorComponent(self):
    ones = [1]*SimpleRemovability.maxcolors
    assert(len(ones) == SimpleRemovability.maxcolors)
    return ones
  def __str__(self):
    return "*"

class DefaultRemoveEffect:
  def __init__(self):
    pass
  def remove(self,x,y,grid):
    grid.getCell(x,y).almond = None
  def __str__(self):
    return "-"

class BombRemoveEffect(DefaultRemoveEffect):
  def __init__(self):
    DefaultRemoveEffect.__init__(self)
  def remove(self,x,y,grid):
    fromx = max(0,x-1)
    fromy = max(0,y-1)
    tox = min(grid.sizex-1,x+1)
    toy = min(grid.sizey-1,y+1)
    for x in range(fromx,tox+1):
      for y in range(fromy,toy+1):
        grid.getCell(x,y).almond = None
  def __str__(self):
    return "X"

class Almond:
  def __init__(self,cell,removability,removeEffect):
    self.cell = cell
    self.removability = removability
    self.removeEffect = removeEffect

  def __str__(self):
    #print "at cell %d %d" % (self.cell.x, self.cell.y)
    #print repr(self.removability)
    #print repr(self.removeEffect)
    return str(self.removability)+"/"+str(self.removeEffect)

class Cell:
  def __init__(self,myx,myy):
    self.x = myx
    self.y = myy
    self.almond = None

  def __str__(self):
    if self.almond:
      return "["+str(self.almond)+"]"
    else:
      return "[   ]"

class Grid:
  """
  Members:
  self.cell: 2-dimensional list of Cell s
    first dimension: row index
    second dimension: column index
  """
  def __init__(self,sizex,sizey):
    self.sizex = sizex
    self.sizey = sizey
    self.cells = [
        [ Cell(x,y) for x in range(0,sizex) ]
        for y in range(0,sizey)
      ]

  def getCell(self,x,y):
    """
    get the cell in the x'th column and the y'th row
    """
    assert(x >= 0 and y >= 0)
    assert(x < self.sizex and y < self.sizey)
    return self.cells[y][x]

  def swapAlmond(self,x1,y1,x2,y2):
    c1 = self.getCell(x1,y1)
    c2 = self.getCell(x2,y2)
    c1.almond, c2.almond = c2.almond, c1.almond

  def __str__(self):
    out = ''
    # go over rows
    for row in self.cells:
      # go over columns
      for cell in row:
        # here we have a cell
        out += str(cell)
      out += '\n'
    return out

  def pullAlmondIfPossible(self,x,y,xplus,yplus):
    #print "pulling into %d/%d with xplus/yplus %d/%d" %(x,y,xplus,yplus)
    thiscell = self.getCell(x,y)
    if thiscell.almond == None:
      # get other almond
      pullfromx = x - xplus
      pullfromy = y - yplus
      while True:
        if pullfromx < 0 or pullfromx >= self.sizex:
          return
        if pullfromy < 0 or pullfromy >= self.sizey:
          return
        othercell = self.getCell(pullfromx,pullfromy)
        if othercell.almond != None:
          thiscell.almond = othercell.almond
          othercell.almond = None
          return
        else:
          pullfromx = pullfromx - xplus
          pullfromy = pullfromy - yplus

  def applyGravity(self,g):
    xplus, yplus = g
    # depending on direction of gravity
    # we have to pull from different directions
    # so we go through all coordinates
    # in a reverse order if necessary
    xcoordinates = range(0,self.sizex)
    if xplus == 1:
      xcoordinates.reverse()
    ycoordinates = range(0,self.sizey)
    if yplus == 1:
      ycoordinates.reverse()
    #print "x coordinates", xcoordinates
    #print "y coordinates", ycoordinates
    for x in xcoordinates:
      for y in ycoordinates:
        self.pullAlmondIfPossible(x,y,xplus,yplus)

class RemovableDetector:
  def __init__(self,grid):
    self.grid = grid
  def removableRight(self,cell):
    return self.removableXY(cell,1,0)
  def removableDown(self,cell):
    return self.removableXY(cell,0,1)
  def removableXY(self,cell,xplus,yplus):
    """
    this function checks removability in the grid,
    starting at fromx/fromy
    going into x/y direction in steps xplus/yplus
    """
    fromx = cell.x
    fromy = cell.y
    #print "at cell %d %d" % (fromx,fromy)
    if fromx > self.grid.sizex-3 and xplus > 0:
      return False
    if fromy > self.grid.sizey-3 and yplus > 0:
      return False
    # get current colors
    # [1, 0, 0, 0] for first color
    # [1, 1, 1, 1] for joker
    cellcolors = []
    for x in range(fromx,fromx+1+xplus*2):
      for y in range(fromy,fromy+1+yplus*2):
        #print "checking %d %d" % (x,y)
        xycell = self.grid.getCell(x,y)
        removability = xycell.almond.removability
        color = removability.getColorComponent()
        cellcolors.append(color)
        #print " got %s" % str(color)
    #print "cellcolors:", cellcolors
    zipcolors = zip(*cellcolors)
    #print "zipped:", zipcolors
    mincolors = map(min,zipcolors)
    #print "minimums:", mincolors
    if max(mincolors) == 1:
      # can remove almond!
      return True
    return False

  def getAllRemovables(self):
    result = []
    for y in range(0,self.grid.sizey):
      for x in range(0,self.grid.sizex):
        cell = self.grid.getCell(x,y)
        if self.removableRight(cell):
          result.append( (x, y) )
          result.append( (x+1, y) )
          result.append( (x+2, y) )
        if self.removableDown(cell):
          result.append( (x, y) )
          result.append( (x, y+1) )
          result.append( (x, y+2) )
    return set(result)

class MoveDetector(RemovableDetector):
  def __init__(self,grid):
    RemovableDetector.__init__(self,grid)
  def getAllMoves(self):
    result = []
    for y in range(0,self.grid.sizey):
      for x in range(0,self.grid.sizex-1):
        # check move from x/y to right
        self.grid.swapAlmond(x,y, x+1,y)
        # TODO instead of checking all removables,
        # we can just check 8 possibilities of new triples
        if len(self.getAllRemovables()) != 0:
          result.append( (x,y,'r') )
        self.grid.swapAlmond(x,y, x+1,y)
    for y in range(0,self.grid.sizey-1):
      for x in range(0,self.grid.sizex):
        # check move from x/y down
        self.grid.swapAlmond(x,y, x,y+1)
        # TODO instead of checking all removables,
        # we can just check 8 possibilities of new triples
        if len(self.getAllRemovables()) != 0:
          result.append( (x,y,'d') )
        self.grid.swapAlmond(x,y, x,y+1)
    return set(result)

class Model:
  def __init__(self):
    self.grid = None

  def getGrid(self):
    return self.grid

  def __str__(self):
    return str(self.grid)

def doRemovingAndGravity(grid,rd, numtypes,showIt=False):
  removable = rd.getAllRemovables()
  while len(removable) > 0:
    for x, y in removable:
      cell = grid.getCell(x, y)
      almond = cell.almond
      if almond:
        almond.removeEffect.remove(x,y,grid)
    if showIt:
      print str(grid)
    refillEmptyCells(grid, numtypes)
    if showIt:
      print str(grid)
    removable = rd.getAllRemovables()

def main():
  m = Model()
  numtypes = 3
  initializeGrid(m,6,8,numtypes)
  print str(m)
  rd = RemovableDetector(m.grid)
  md = MoveDetector(m.grid)
  g = (0,1)
  #g = (random.randint(-1,1), random.randint(-1,1))
  print "our gravity is "+str(g)

  doRemovingAndGravity(m.grid, rd, numtypes)
  print "starting grid (no moves should be possible)"
  print str(m)

  possiblemoves = md.getAllMoves()
  print "possible moves"
  print repr(possiblemoves)

  if len(possiblemoves) > 0:
    domove = list(possiblemoves)[0]
    print "doing move %s" % repr(domove)
    x, y, direction = domove
    if direction == 'r':
      m.grid.swapAlmond(x, y, x+1, y)
    else:
      m.grid.swapAlmond(x, y, x, y+1)
    
    print str(m)
    doRemovingAndGravity(m.grid, rd, numtypes, showIt=True)
    print "after removing and gravity"
    print str(m)

  else:
    print "no moves possible"


  #for iteration in range(0,10):
  #  removable = rd.getAllRemovables()
  #  print removable
  #  for x, y in removable:
  #    cell = m.grid.getCell(x, y)
  #    almond = cell.almond
  #    if almond:
  #      almond.removeEffect.remove(x,y,m.grid)
  #  print str(m)
  #  m.grid.applyGravity(g)
  #  print str(m)
  #  print "now refilling"
  #  refillEmptyCells(m.grid, numtypes)
  #  print str(m)

if __name__ == '__main__':
  myseed = int(time.time()*100.0)
  print "my seed: %d" % myseed
  random.seed(myseed)
  main()
